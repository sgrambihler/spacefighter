
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	//Check if the ship still has a delay time
	if (m_delaySeconds > 0)
	{
		//Subtract the time since the last frame from the ship's delay
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		//If the delay is now 0 or lower, the ship is made active
		if (m_delaySeconds <= 0)
		{
			GameObject::Activate();
		}
	}

	//If the ship is active, this keeps track of how long it has been active
	//If it has been active for at least two seconds and is no longer on screen, the  ship is deactivated
	if (IsActive())
	{
		m_activationSeconds += pGameTime->GetTimeElapsed();
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}

	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}