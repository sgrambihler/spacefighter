

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	//Load the textures for the enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	//Set the number of enemies that will be in the level
	const int COUNT = 21;

	//Create an array of doubles to hold the starting position of each enemy ship
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	//Create another array of doubles to hold the time delay for each enemy ship to spawn
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	//Set the amount of time before the first enemy spawns
	float delay = .5; // start delay

	//Create an empty vector
	Vector2 position;

	//Loop once for each enemy ship
	for (int i = 0; i < COUNT; i++)
	{
		//Add the ship's time delay to the start time delay to get the total delay
		delay += delays[i];
		//Create a vector using the ships given x postion, and the center of the ships texture
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		//Create a new ship on the heap
		BioEnemyShip *pEnemy = new BioEnemyShip();
		//Pass the new ship it's texture, the current level, it's position and it's time delay
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		//Add the ship to the game
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

